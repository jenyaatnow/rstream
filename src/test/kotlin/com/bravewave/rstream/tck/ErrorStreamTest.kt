package com.bravewave.rstream.tck

import com.bravewave.rstream.RStream
import org.reactivestreams.tck.TestEnvironment
import org.reactivestreams.tck.flow.FlowPublisherVerification
import org.testng.annotations.Test
import java.util.concurrent.Flow

@Test
class ErrorStreamTest : FlowPublisherVerification<Long>(TestEnvironment()) {

    override fun createFlowPublisher(elements: Long): Flow.Publisher<Long>? {
        return RStream.error(RuntimeException("Error Publisher"))
    }

    override fun createFailedFlowPublisher(): Flow.Publisher<Long> {
        return RStream.error(RuntimeException("Error Publisher"))
    }

    override fun maxElementsFromPublisher() = 0L
}
