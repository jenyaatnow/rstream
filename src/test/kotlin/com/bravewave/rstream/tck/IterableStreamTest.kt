package com.bravewave.rstream.tck

import com.bravewave.rstream.source.IterableStream
import org.reactivestreams.tck.TestEnvironment
import org.reactivestreams.tck.flow.FlowPublisherVerification
import org.testng.annotations.Test
import java.util.concurrent.Flow


@Test
class IterableStreamTest : FlowPublisherVerification<Long>(TestEnvironment()) {

    override fun createFlowPublisher(elements: Long): Flow.Publisher<Long> {
        return IterableStream(0 until elements)
    }

    override fun createFailedFlowPublisher(): Flow.Publisher<Long>? {
        return null
    }
}
