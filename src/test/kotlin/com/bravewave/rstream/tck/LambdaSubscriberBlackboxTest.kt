package com.bravewave.rstream.tck

import com.bravewave.rstream.subscriber.LambdaSubscriber
import org.reactivestreams.tck.TestEnvironment
import org.reactivestreams.tck.flow.FlowSubscriberBlackboxVerification
import org.testng.annotations.Test
import java.util.concurrent.Flow

@Test
class LambdaSubscriberBlackboxTest : FlowSubscriberBlackboxVerification<Int>(TestEnvironment()) {

    override fun createElement(element: Int): Int {
        return 1
    }

    override fun createFlowSubscriber(): Flow.Subscriber<Int> {
        return LambdaSubscriber({}, {}, {}, {}, 100L)
    }
}
