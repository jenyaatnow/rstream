package com.bravewave.rstream.tck

import com.bravewave.rstream.subscriber.LambdaSubscriber
import org.reactivestreams.tck.TestEnvironment
import org.reactivestreams.tck.flow.FlowSubscriberWhiteboxVerification
import org.testng.annotations.Test
import java.util.concurrent.Flow

@Test
class LambdaSubscriberWhiteboxTest : FlowSubscriberWhiteboxVerification<Int>(TestEnvironment()) {

    override fun createFlowSubscriber(probe: WhiteboxSubscriberProbe<Int>?): Flow.Subscriber<Int> {
        return object : Flow.Subscriber<Int> {
            val subscriber = LambdaSubscriber<Int>({}, {}, {}, {}, Long.MAX_VALUE)

            override fun onComplete() {
                subscriber.onComplete()
                probe?.registerOnComplete()
            }

            override fun onSubscribe(s: Flow.Subscription?) {
                subscriber.onSubscribe(s)
                probe?.registerOnSubscribe(object : SubscriberPuppet {
                    override fun signalCancel() {
                        s?.cancel()
                    }

                    override fun triggerRequest(elements: Long) {
                        s?.request(elements)
                    }
                })
            }

            override fun onNext(element: Int?) {
                subscriber.onNext(element)
                probe?.registerOnNext(element)
            }

            override fun onError(e: Throwable?) {
                subscriber.onError(e)
                probe?.registerOnError(e)
            }
        }
    }

    override fun createElement(element: Int): Int {
        return  element
    }
}