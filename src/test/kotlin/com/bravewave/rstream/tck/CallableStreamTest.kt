package com.bravewave.rstream.tck

import com.bravewave.rstream.RStream
import org.reactivestreams.tck.TestEnvironment
import org.reactivestreams.tck.flow.FlowPublisherVerification
import org.testng.annotations.Test
import java.util.concurrent.Flow

@Test
class CallableStreamTest : FlowPublisherVerification<Long>(TestEnvironment()) {

    override fun createFlowPublisher(elements: Long): Flow.Publisher<Long>? {
        return RStream.fromCallable { elements }
    }

    override fun createFailedFlowPublisher(): Flow.Publisher<Long> {
        return RStream.fromCallable { throw RuntimeException("Callable exception") }
    }

    override fun maxElementsFromPublisher() = 1L
}
