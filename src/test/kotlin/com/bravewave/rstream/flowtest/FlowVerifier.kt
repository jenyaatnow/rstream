package com.bravewave.rstream.flowtest

import java.util.concurrent.Flow

class FlowVerifier<T> private constructor(private val subscriber: VerificationSubscriber<T>) {

    companion object {
        fun <T> of(source: Flow.Publisher<T>): FlowVerifier<T> {
            val subscriber = VerificationSubscriber<T>()
            val flowVerifier = FlowVerifier(subscriber)
            source.subscribe(subscriber)
            return flowVerifier.verifySubscription()
        }
    }

    fun verifySubscription(): FlowVerifier<T> {
        subscriber.verifySubscription()
        return this
    }

    fun verifyElement(element: T): FlowVerifier<T> {
        subscriber.verifyElement(element)
        return this
    }

    fun verifyCompletion(): FlowVerifier<T> {
        subscriber.verifyCompletion()
        subscriber.verifyNoMoreSignals()
        return this
    }

    fun verifyError(e: Throwable): FlowVerifier<T> {
        subscriber.verifyError(e)
        subscriber.verifyNoMoreSignals()
        return this
    }

    fun verifyNoMoreSignals(): FlowVerifier<T> {
        subscriber.verifyNoMoreSignals()
        return this
    }
}
