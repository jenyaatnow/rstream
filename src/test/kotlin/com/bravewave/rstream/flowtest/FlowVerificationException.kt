package com.bravewave.rstream.flowtest

class FlowVerificationException(message: String) : RuntimeException(message)
