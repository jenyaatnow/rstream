package com.bravewave.rstream.flowtest

import com.bravewave.rstream.subscriber.LambdaSubscriber
import org.testng.Assert
import java.util.concurrent.Flow

internal class VerificationSubscriber<T>(
    private val signals: MutableList<Signal<T>> = mutableListOf(),
    baseSubscriber: Flow.Subscriber<T> = LambdaSubscriber(
        { signals.add(Signal.onNext(it)) },
        { signals.add(Signal.onComplete()) },
        { signals.add(Signal.onError(it)) },
        { signals.add(Signal.onSubscribe(it)) },
        Long.MAX_VALUE
    )
) : Flow.Subscriber<T> by baseSubscriber {

    private lateinit var iter: Iterator<Signal<T>>

    fun verifySubscription() {
        iter = signals.iterator()

        verifyNextSignalObtaining("onSubscribe()")

        val actual = iter.next()
        Assert.assertTrue(
            actual.isOnSubscribe(),
            "Unexpected signal '$actual' while expected 'onSubscribe()'.\n"
        )
    }

    fun verifyElement(element: T) {
        verifyNextSignalObtaining("onNext($element)")

        val actualSignal = iter.next()
        Assert.assertTrue(
            actualSignal.isOnNext(),
            "Unexpected signal '$actualSignal' while expected 'onNext($element)'.\n"
        )

        val actualElement = actualSignal.getElement()
        Assert.assertSame(
            actualElement,
            element,
            "Unexpected element '$actualElement' while expected '$element'.\n"
        )
    }

    fun verifyCompletion() {
        verifyNextSignalObtaining("onComplete()")

        val actual = iter.next()
        Assert.assertTrue(
            actual.isOnComplete(),
            "Unexpected signal '$actual' while expected 'onComplete()'.\n"
        )
    }

    fun verifyError(e: Throwable) {
        verifyNextSignalObtaining("onError($e)")

        val actual = iter.next()
        Assert.assertTrue(
            actual.isOnError(),
            "Unexpected signal '$actual' while expected 'onError($e)'.\n"
        )

        // TODO assert exception instance
    }

    fun verifyNoMoreSignals() {
        val hasNext = iter.hasNext()
        val restSignals = mutableListOf<Signal<T>>()
        while (iter.hasNext()) {
            restSignals.add(iter.next())
        }

        Assert.assertFalse(
            hasNext,
            "No more signals expected but found ${restSignals.size}:\n" +
                    "${restSignals.joinToString("\n")}\n"
        )
    }

    private fun verifyNextSignalObtaining(expected: String) {
        Assert.assertTrue(iter.hasNext(), "No following signals received while expected '$expected'.\n")
    }
}
