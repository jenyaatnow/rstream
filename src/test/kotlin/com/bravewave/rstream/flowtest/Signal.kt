package com.bravewave.rstream.flowtest

import java.util.concurrent.Flow

internal interface Signal<ElementType> {

    companion object {
        fun <T> onSubscribe(s: Flow.Subscription): Signal<T> = OnSubscribe(s)
        fun <T> onNext(item: T): Signal<T> = OnNext(item)
        fun <T> onError(e: Throwable): Signal<T> = OnError(e)
        fun <T> onComplete(): Signal<T> = OnComplete()
    }


    fun isOnSubscribe() = this is OnSubscribe
    fun isOnNext() = this is OnNext
    fun isOnError() = this is OnError
    fun isOnComplete() = this is OnComplete

    fun getElement(): ElementType {
        return (this as? OnNext)?.item
            ?: throw FlowVerificationException("Current signal is '$this' while 'onNext()' expected")
    }


    private class OnSubscribe<T>(private val s: Flow.Subscription) : Signal<T> {
        override fun toString(): String {
            return "onSubscribe($s)"
        }
    }

    private class OnNext<T>(val item: T) : Signal<T> {
        override fun toString(): String {
            return "onNext($item)"
        }
    }

    private class OnComplete<T> : Signal<T> {
        override fun toString(): String {
            return "onComplete()"
        }
    }

    private class OnError<T>(private val e: Throwable?) : Signal<T> {
        override fun toString(): String {
            return "onError($e)"
        }
    }

}