package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.annotations.Test

class SwitchIfEmptyOperatorTest {

    @Test
    fun `upstream error should be passed down`() {
        val e = RuntimeException("Upstream exception")
        val switchIfEmpty = RStream.error<Int>(e).switchItEmpty(RStream.single(1))

        FlowVerifier.of(switchIfEmpty)
            .verifyError(e)
    }

    @Test
    fun `alternate shouldn't work when upstream has elements`() {
        val upstreamElement = 1
        val alternateElement = 2
        val switchItEmpty = RStream.single(upstreamElement).switchItEmpty(RStream.single(alternateElement))

        FlowVerifier.of(switchItEmpty)
            .verifyElement(upstreamElement)
            .verifyCompletion()
    }

    @Test
    fun `alternate should emmit when upstream has no elements`() {
        val alternateElement = 2
        val switchItEmpty = RStream.empty<Int>().switchItEmpty(RStream.single(alternateElement))

        FlowVerifier.of(switchItEmpty)
            .verifyElement(alternateElement)
            .verifyCompletion()
    }

    @Test
    fun `empty alternate stream should cause onComplete()`() {
        val switchItEmpty = RStream.empty<Int>().switchItEmpty(RStream.empty())

        FlowVerifier.of(switchItEmpty)
            .verifyCompletion()
    }

    @Test
    fun `error in alternate stream should cause onError()`() {
        val e = RuntimeException("Alternate stream exception")
        val switchItEmpty = RStream.empty<Int>().switchItEmpty(RStream.error(e))

        FlowVerifier.of(switchItEmpty)
            .verifyError(e)
    }
}