package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.annotations.Test

@Suppress("NestedLambdaShadowedImplicitParameter")
class ReshapeOperatorTest {

    @Test
    fun `upstream error should be passed down`() {
        val e = RuntimeException("Upstream exception")
        val reshape = RStream.error<Int>(e).reshape { it.filter { it == 1 } }

        FlowVerifier.of(reshape)
            .verifyError(e)
    }

    @Test
    fun `reshaper shouldn't affect empty stream`() {
        val reshape = RStream.empty<Int>().reshape { it.map { it * 10 } }

        FlowVerifier.of(reshape)
            .verifyCompletion()
    }

    @Test
    fun `reshaper should reshape entire upstream`() {
        val reshape = RStream.fromIterable(listOf(1, 2))
            .reshape {
                it.map { it * 10 }
                  .filter { it == 10 }
            }

        FlowVerifier.of(reshape)
            .verifyElement(10)
            .verifyCompletion()
    }

    @Test
    fun `exception at reshaper should cause onError()`() {
        val e = RuntimeException("Reshaper exception")
        val reshape = RStream.single(1).reshape <Int> { throw e }

        FlowVerifier.of(reshape)
            .verifyError(e)
    }

    @Test
    fun `exception at reshaper's nested operator should cause onError()`() {
        val e = RuntimeException("Mapper exception")
        val reshape = RStream.single(1).reshape { it.map { throw e } }

        FlowVerifier.of(reshape)
            .verifyError(e)
    }
}