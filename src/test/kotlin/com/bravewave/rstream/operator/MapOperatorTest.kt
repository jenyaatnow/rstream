package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.annotations.Test

class MapOperatorTest {

    @Test
    fun `upstream error should be passed down`() {
        val e = RuntimeException("Upstream exception")
        val map = RStream.error<Int>(e).map { it }

        FlowVerifier.of(map)
            .verifyError(e)
    }

    @Test
    fun `mapper should transform each element`() {
        val map = RStream.fromIterable(listOf(1, 2, 3)).map { it * 10 }

        FlowVerifier.of(map)
            .verifyElement(10)
            .verifyElement(20)
            .verifyElement(30)
            .verifyCompletion()
    }

    @Test
    fun `mapper shouldn't affect empty stream`() {
        val map = RStream.empty<Int>().map { throw RuntimeException() }

        FlowVerifier.of(map)
            .verifyCompletion()
    }

    @Test
    fun `exception at mapper should cause onError()`() {
        val e = RuntimeException("Mapper exception")
        val map = RStream.fromIterable(listOf(1, 2, 3))
            .map { throw e }

        FlowVerifier.of(map)
            .verifyError(e)
    }
}