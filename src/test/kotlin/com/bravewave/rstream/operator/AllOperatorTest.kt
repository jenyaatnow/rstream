package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.annotations.Test

class AllOperatorTest {

    @Test
    fun `upstream error should be passed down`() {
        val e = RuntimeException("Upstream exception")
        val all = RStream.error<Int>(e).all { it == 1 }

        FlowVerifier.of(all)
            .verifyError(e)
    }

    @Test
    fun `empty stream should resolve to true`() {
        val all = RStream.empty<Int>().all { it == 1 }

        FlowVerifier.of(all)
            .verifyElement(true)
            .verifyCompletion()
    }

    @Test
    fun `stream should resolve to true when all elements match predicate`() {
        val all = RStream.fromIterable(listOf(1, 1, 1)).all { it == 1 }

        FlowVerifier.of(all)
            .verifyElement(true)
            .verifyCompletion()
    }

    @Test
    fun `stream should resolve to false when at least one element doesn't match predicate`() {
        val all = RStream.fromIterable(listOf(1, 2, 1)).all { it == 1 }

        FlowVerifier.of(all)
            .verifyElement(false)
            .verifyCompletion()
    }

    @Test
    fun `exception at filter predicate should cause onError()`() {
        val e = RuntimeException("Predicate exception")
        val all = RStream.fromIterable(listOf(1, 2, 3, 4)).all { throw e }

        FlowVerifier.of(all)
            .verifyError(e)
    }
}
