package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.Assert
import org.testng.annotations.Test

class OnErrorCatchOperatorTest {

    @Test
    fun `expected exception should be handled and stream should continue`() {
        val expected = FirstTestException("Expected exception")
        lateinit var actual: Exception

        val onErrorCatch = RStream.of(1, 2, 3)
            .map { if (it == 2) throw expected else it }
            .onErrorCatch(FirstTestException::class) { actual = it }

        FlowVerifier.of(onErrorCatch)
            .verifyElement(1)
            .verifyElement(3)
            .verifyCompletion()

        Assert.assertEquals(actual, expected)
    }

    @Test
    fun `unexpected exception should cause onError()`() {
        val unexpected = RuntimeException("Unexpected exception")
        var actual: Exception? = null

        val onErrorCatch = RStream.of(1, 2, 3)
            .map { if (it == 2) throw RuntimeException() else it }
            .onErrorCatch(FirstTestException::class) { actual = it }

        FlowVerifier.of(onErrorCatch)
            .verifyElement(1)
            .verifyError(unexpected)

        Assert.assertTrue(actual == null)
    }

    @Test
    fun `each exception of expected type should be handled by appropriate handler`() {
        val firstExpected = FirstTestException("First expected exception")
        lateinit var firstActual: FirstTestException

        val secondExpected = SecondTestException("Second expected exception")
        lateinit var secondActual: SecondTestException

        val onErrorCatch = RStream.of(1, 2, 3, 4)
            .map { if (it == 2) throw firstExpected else if (it == 3) throw secondExpected else it }
            .onErrorCatch(FirstTestException::class) { firstActual = it }
            .onErrorCatch(SecondTestException::class) { secondActual = it }

        FlowVerifier.of(onErrorCatch)
            .verifyElement(1)
            .verifyElement(4)
            .verifyCompletion()

        Assert.assertEquals(firstActual, firstExpected)
        Assert.assertEquals(secondActual, secondExpected)
    }

    @Test
    fun `each exception of same type should be handled`() {
        val actual = mutableListOf<Exception>()
        val firstExpected = FirstTestException("First expected exception")
        val secondExpected = FirstTestException("Second expected exception")

        val onErrorCatch = RStream.of(1, 2, 3, 4)
            .map { if (it == 2) throw firstExpected else if (it == 3) throw secondExpected else it }
            .onErrorCatch(FirstTestException::class) { actual.add(it) }

        FlowVerifier.of(onErrorCatch)
            .verifyElement(1)
            .verifyElement(4)
            .verifyCompletion()

        Assert.assertTrue(actual.contains(firstExpected))
        Assert.assertTrue(actual.contains(secondExpected))
    }

    @Test
    fun `handler shouldn't affect empty stream`() {
        val actual = mutableListOf<Exception>()

        val onErrorCatch = RStream.empty<Int>()
            .onErrorCatch(java.lang.Exception::class) { actual.add(it) }

        FlowVerifier.of(onErrorCatch)
            .verifyCompletion()

        Assert.assertTrue(actual.isEmpty())
    }

    @Test
    fun `handler shouldn't affect correct stream`() {
        val actual = mutableListOf<Exception>()

        val onErrorCatch = RStream.single(1)
            .onErrorCatch(java.lang.Exception::class) { actual.add(it) }

        FlowVerifier.of(onErrorCatch)
            .verifyElement(1)
            .verifyCompletion()

        Assert.assertTrue(actual.isEmpty())
    }

    @Test
    fun `exception in handler should cause onError()`() {
        val streamException = RuntimeException("Stream exception")
        val handlerException = RuntimeException("Handler exception")

        val onErrorCatch = RStream.error<Int>(streamException)
            .onErrorCatch(java.lang.Exception::class) { throw handlerException }

        FlowVerifier.of(onErrorCatch)
            .verifyError(handlerException)
    }

    private data class FirstTestException(private val msg: String) : RuntimeException(msg)
    private data class SecondTestException(private val msg: String) : RuntimeException(msg)
}