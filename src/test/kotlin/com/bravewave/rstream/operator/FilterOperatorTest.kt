package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.annotations.Test

class FilterOperatorTest {

    @Test
    fun `upstream error should be passed down`() {
        val e = RuntimeException("Upstream exception")
        val filter = RStream.error<Int>(e).filter { it == 1 }

        FlowVerifier.of(filter)
            .verifyError(e)
    }

    @Test
    fun `2 of 3 elements should pass`() {
        val filter = RStream.fromIterable(listOf("1", "", "2")).filter { it != "" }

        FlowVerifier.of(filter)
            .verifyElement("1")
            .verifyElement("2")
            .verifyCompletion()
    }

    @Test
    fun `all elements should pass`() {
        val filter = RStream.fromIterable(listOf("1", "2", "3")).filter { it != "" }

        FlowVerifier.of(filter)
            .verifyElement("1")
            .verifyElement("2")
            .verifyElement("3")
            .verifyCompletion()
    }

    @Test
    fun `none elements should pass`() {
        val filter = RStream.fromIterable(listOf("1", "2", "3")).filter { it == "" }

        FlowVerifier.of(filter)
            .verifyCompletion()
    }

    @Test
    fun `exception at filter predicate should cause onError()`() {
        val e = RuntimeException("Predicate exception")
        val filter = RStream.fromIterable(listOf("1", "2", "3")).filter { throw e }

        FlowVerifier.of(filter)
            .verifyError(e)
    }
}
