package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.flowtest.FlowVerifier
import org.testng.annotations.Test

class AnyOperatorTest {

    @Test
    fun `upstream error should be passed down`() {
        val e = RuntimeException("Upstream exception")
        val any = RStream.error<Int>(e).any { it == 1 }

        FlowVerifier.of(any)
            .verifyError(e)
    }

    @Test
    fun `empty stream should resolve to false`() {
        val any = RStream.empty<Int>().any { it == 3 }

        FlowVerifier.of(any)
            .verifyElement(false)
            .verifyCompletion()
    }

    @Test
    fun `stream with suitable element should resolve to true`() {
        val any = RStream.fromIterable(listOf(1, 2, 3, 4)).any { it == 3 }

        FlowVerifier.of(any)
            .verifyElement(true)
            .verifyCompletion()
    }

    @Test
    fun `stream with no suitable elements should resolve to false`() {
        val any = RStream.fromIterable(listOf(1, 2, 3, 4)).any { it == 5 }

        FlowVerifier.of(any)
            .verifyElement(false)
            .verifyCompletion()
    }

    @Test
    fun `exception at filter predicate should cause onError()`() {
        val e = RuntimeException("Predicate exception")
        val any = RStream.fromIterable(listOf(1, 2, 3, 4)).any { throw e }

        FlowVerifier.of(any)
            .verifyError(e)
    }
}