package com.bravewave.rstream.source

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.onFalse
import com.bravewave.rstream.utils.onTrue
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong

internal class IterableStream<I : Any>(items: Iterable<I>) : RStream<I> {

    val iterator = items.iterator()

    override fun subscribe(subscriber: RSubscriber<in I>) {
        subscriber.onSubscribe(IterableSubscription(subscriber))
    }

    private inner class IterableSubscription(private val subscriber: RSubscriber<in I>) : RSubscription {

        // TODO should divide cancelled and completed
        private val cancelledOrCompleted = AtomicBoolean(false)
        private val unbounded = AtomicBoolean(false)
        private val running = AtomicBoolean(false)
        private val requestCounter = AtomicLong(0)

        override fun cancel() {
            cancelledOrCompleted.set(true)
            running.set(false)
        }

        override fun resume(n: Long) {
            cancelledOrCompleted.set(false)
            request(n)
        }

        override fun request(n: Long) {
            (n < 1).onTrue { return subscriber.onError(IllegalArgumentException("Illegal request: $n")) }

            val overflowed = requestCounter.addAndGet(n) < requestCounter.get()
            overflowed.onTrue { unbounded.set(true) }
            running.compareAndSet(false, true).onFalse { return }
            emmit()
        }

        private fun emmit() {
            while (!cancelledOrCompleted.get()) {
                iterator.hasNext().onFalse {
                    cancelledOrCompleted.set(true)
                    subscriber.onComplete()
                    return
                }

                subscriber.onNext(iterator.next())

                val requestSatisfied = !unbounded.get() && requestCounter.decrementAndGet() < 1
                requestSatisfied.onTrue { return running.set(false) }
            }
        }
    }
}
