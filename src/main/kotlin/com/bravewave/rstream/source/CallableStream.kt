package com.bravewave.rstream.source

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.onFalse
import com.bravewave.rstream.utils.onTrue
import java.util.concurrent.atomic.AtomicBoolean

internal class CallableStream<I : Any>(callable: () -> I) : RStream<I> {

    private val element: I by lazy(callable)

    override fun subscribe(subscriber: RSubscriber<in I>) {
        subscriber.onSubscribe(CallableSubscription(subscriber))
        kotlin.runCatching { element }.onFailure { subscriber.onError(it) }
    }

    private inner class CallableSubscription(private val subscriber: RSubscriber<in I>) : RSubscription {

        private val cancelledOrCompleted = AtomicBoolean(false)

        override fun cancel() {
            cancelledOrCompleted.set(true)
        }

        override fun request(n: Long) {
            (n < 1).onTrue { return subscriber.onError(IllegalArgumentException("Illegal request: $n")) }

            (cancelledOrCompleted.compareAndSet(false, true)).onFalse { return }

            subscriber.onNext(element)
            subscriber.onComplete()
        }

        override fun resume(n: Long) {
            // TODO write tests
            subscriber.onComplete()
        }
    }
}
