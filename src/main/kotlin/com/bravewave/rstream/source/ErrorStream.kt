package com.bravewave.rstream.source

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription

internal class ErrorStream<O : Any>(private val e: Exception) : RStream<O> {

    override fun subscribe(subscriber: RSubscriber<in O>) {
        subscriber.onSubscribe(object : RSubscription {
            override fun cancel() {}
            override fun request(n: Long) {}
            override fun resume(n: Long) { subscriber.onComplete() } // TODO write tests
        })
        subscriber.onError(e)
    }
}
