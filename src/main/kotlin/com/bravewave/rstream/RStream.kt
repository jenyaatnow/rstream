package com.bravewave.rstream

import com.bravewave.rstream.operator.*
import com.bravewave.rstream.source.CallableStream
import com.bravewave.rstream.source.ErrorStream
import com.bravewave.rstream.source.IterableStream
import com.bravewave.rstream.subscriber.FlowSubscriberAdapter
import com.bravewave.rstream.subscriber.LambdaSubscriber
import java.util.concurrent.Flow
import kotlin.reflect.KClass

interface RStream<I : Any> : Flow.Publisher<I> {

    companion object {

        fun <I : Any> empty(): RStream<I> {
            return IterableStream(listOf())
        }

        fun <I : Any> error(e: java.lang.Exception): RStream<I> {
            return ErrorStream(e)
        }

        fun <I : Any> fromCallable(callable: () -> I): RStream<I> {
            return CallableStream(callable)
        }

        fun <I : Any> fromIterable(items: Iterable<I>): RStream<I> {
            return IterableStream(items)
        }

        fun <I : Any> of(vararg items: I): RStream<I> {
            return IterableStream(items.asIterable())
        }

        fun <I : Any> single(item: I): RStream<I> {
            return IterableStream(listOf(item))
        }
    }

    fun all(predicate: (I) -> Boolean): RStream<Boolean> {
        return AllOperator(this, predicate)
    }

    fun any(predicate: (I) -> Boolean): RStream<Boolean> {
        return AnyOperator(this, predicate)
    }

    fun filter(predicate: (I) -> Boolean): RStream<I> {
        return FilterOperator(this, predicate)
    }

    fun log() : RStream<I> {
        return LogOperator(this)
    }

    fun <O : Any> map(mapper: (I) -> O): RStream<O> {
        return MapOperator(this, mapper)
    }

    fun <E : Exception> onErrorCatch(kClass: KClass<E>, handler: (E) -> Unit): RStream<I> {
        return OnErrorCatchOperator(this, kClass, handler)
    }

    fun <O : Any> reshape(reshapeFunc: (RStream<I>) -> RStream<O>): RStream<O> {
        return kotlin.runCatching { reshapeFunc(this) }
            .getOrElse { ErrorStream(it as Exception) }
    }

    fun switchItEmpty(alternate: RStream<I>): RStream<I> {
        return SwitchIfEmptyOperator(this, alternate)
    }

    fun subscribe(subscriber: RSubscriber<in I>)

    fun subscribe(itemConsumer: (I) -> Unit = {},
                  completeConsumer: () -> Unit = {},
                  errorConsumer: (Throwable) -> Unit = { throw it },
                  subscriptionConsumer: (Flow.Subscription) -> Unit = {},
                  request: Long = Long.MAX_VALUE)
    {
        subscribe(LambdaSubscriber(itemConsumer, completeConsumer, errorConsumer, subscriptionConsumer, request))
    }

    override fun subscribe(subscriber: Flow.Subscriber<in I>?) {
        subscriber ?: throw NullPointerException("Subscriber mustn't be null")
        subscribe(FlowSubscriberAdapter(subscriber))
    }
}
