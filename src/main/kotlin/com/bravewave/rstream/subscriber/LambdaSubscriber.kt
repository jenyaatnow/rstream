package com.bravewave.rstream.subscriber

import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.either
import com.bravewave.rstream.utils.onTrue
import java.util.concurrent.Flow
import java.util.concurrent.atomic.AtomicLong

internal class LambdaSubscriber<I>(private val itemConsumer: (I) -> Unit,
                                   private val completeConsumer: () -> Unit,
                                   private val errorConsumer: (Throwable) -> Unit,
                                   private val subscriptionConsumer: (Flow.Subscription) -> Unit,
                                   private val request: Long) : RSubscriber<I> {

    private val seen = AtomicLong(0)
    private lateinit var subscription: Flow.Subscription

    override fun onComplete() {
        completeConsumer()
    }

    override fun onSubscribe(s: RSubscription) {
        handleOnSubscribe(s)
    }

    override fun onSubscribe(s: Flow.Subscription?) {
        s ?: throw NullPointerException("Subscription shouldn't be null")
        handleOnSubscribe(s)
    }

    private fun handleOnSubscribe(s: Flow.Subscription) {
        ::subscription.isInitialized.either(
            { s.cancel() },
            {
                this.subscription = s
                subscriptionConsumer(s)
                s.request(request)
            }
        )
    }

    override fun onNext(item: I?) {
        item ?: throw NullPointerException()

        kotlin.runCatching { itemConsumer(item) }
            .onSuccess {
                val seenVal = seen.incrementAndGet()
                (seenVal >= request).onTrue { // todo make fast path for Long.MAX request
                    seen.set(0)
                    subscription.request(request)
                }
            }.onFailure { onError(it) }
    }

    override fun onError(throwable: Throwable?) {
        throwable ?: throw NullPointerException("Exception shouldn't be null")
        errorConsumer(throwable)
    }
}
