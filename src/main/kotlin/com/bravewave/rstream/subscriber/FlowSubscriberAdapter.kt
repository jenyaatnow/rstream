package com.bravewave.rstream.subscriber

import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import java.util.concurrent.Flow

internal class FlowSubscriberAdapter<I>(private val flowSubscriber: Flow.Subscriber<I>) : RSubscriber<I> {

    override fun onComplete() {
        flowSubscriber.onComplete()
    }

    override fun onSubscribe(s: RSubscription) {
        flowSubscriber.onSubscribe(s)
    }

    override fun onSubscribe(s: Flow.Subscription?) {
        s?.run { flowSubscriber.onSubscribe(FlowSubscriptionAdapter(this)) }
            ?: flowSubscriber.onSubscribe(null)
    }

    override fun onNext(item: I) {
        flowSubscriber.onNext(item)
    }

    override fun onError(throwable: Throwable) {
        flowSubscriber.onError(throwable)
    }

    private class FlowSubscriptionAdapter(private val flowSubscription: Flow.Subscription) : RSubscription {
        override fun cancel() {
            flowSubscription.cancel()
        }

        override fun request(n: Long) {
            flowSubscription.request(n)
        }

        override fun resume(n: Long) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }
}
