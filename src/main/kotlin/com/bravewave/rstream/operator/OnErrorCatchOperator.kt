package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.either
import kotlin.reflect.KClass

internal class OnErrorCatchOperator<I : Any, E : Exception>(private val source: RStream<I>,
                                                            private val kClass: KClass<E>,
                                                            private val handler: (E) -> Unit) : RStream<I> {

    override fun subscribe(subscriber: RSubscriber<in I>) {
        source.subscribe(OnErrorCatchSubscriber(subscriber))
    }

    private inner class OnErrorCatchSubscriber(private val subscriber: RSubscriber<in I>) : RSubscriber<I> {

        private lateinit var subscription: RSubscription

        override fun onComplete() {
            subscriber.onComplete()
        }

        override fun onSubscribe(s: RSubscription) {
            subscription = s
            subscriber.onSubscribe(s)
        }

        override fun onNext(element: I) {
            subscriber.onNext(element)
        }

        override fun onError(throwable: Throwable) {
            (throwable::class == kClass).either(
                {
                    kotlin.runCatching {
                        @Suppress("UNCHECKED_CAST")
                        handler(throwable as E)
                        subscription.resume(1)
                    }.onFailure { subscriber.onError(it) }
                },
                {
                    subscriber.onError(throwable)
                }
            )
        }
    }
}
