package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.either

internal class FilterOperator<I : Any>(private val source: RStream<I>,
                                       private val predicate: (I) -> Boolean) : RStream<I> {

    override fun subscribe(subscriber: RSubscriber<in I>) {
        source.subscribe(FilterSubscriber(subscriber))
    }

    private inner class FilterSubscriber(private val subscriber: RSubscriber<in I>): RSubscriber<I> {

        private lateinit var subscription: RSubscription

        override fun onComplete() {
            subscriber.onComplete()
        }

        override fun onSubscribe(s: RSubscription) {
            subscription = s
            subscriber.onSubscribe(s)
        }

        override fun onNext(item: I) {
            kotlin.runCatching {
                val passed = predicate(item)
                passed.either({ subscriber.onNext(item) }, { subscription.request(1) })
            }.onFailure {
                subscription.cancel()
                onError(it)
            }
        }

        override fun onError(throwable: Throwable) {
            subscriber.onError(throwable)
        }
    }
}
