package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.either
import com.bravewave.rstream.utils.onTrue

internal class SwitchIfEmptyOperator<I : Any>(private val source: RStream<I>,
                                              private val alternate: RStream<I>) : RStream<I> {

    override fun subscribe(subscriber: RSubscriber<in I>) {
        source.subscribe(SwitchIfEmptySubscriber(subscriber))
    }

    private inner class SwitchIfEmptySubscriber(private val subscriber: RSubscriber<in I>): RSubscriber<I> {

        @Volatile private var firstIsEmpty = true
        @Volatile private var first = true

        @Volatile private lateinit var subscription: SwitchIfEmptySubscription

        override fun onComplete() {
            (first && firstIsEmpty).either(
                { first = false; alternate.subscribe(this) },
                { subscriber.onComplete() }
            )
        }

        override fun onSubscribe(s: RSubscription) {
            first.either(
                {
                    subscription = SwitchIfEmptySubscription(s)
                    subscriber.onSubscribe(subscription)
                },
                { subscription.switchSubscription(s) }
            )
        }

        override fun onNext(item: I) {
            first.onTrue { firstIsEmpty = false }
            subscriber.onNext(item)
        }

        override fun onError(throwable: Throwable) {
            subscription.cancel()
            subscriber.onError(throwable)
        }

    }

    private class SwitchIfEmptySubscription(@Volatile private var subscription: RSubscription): RSubscription {

        @Volatile private var req: Long = 0

        fun switchSubscription(s: RSubscription) {
            subscription = s
            subscription.request(req)
        }

        override fun cancel() {
            subscription.cancel()
        }

        override fun request(n: Long) {
            req = n
            subscription.request(n)
        }

        override fun resume(n: Long) {
            subscription.resume(n)
        }
    }
}
