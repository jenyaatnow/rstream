package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import com.bravewave.rstream.utils.onTrue

internal class AnyOperator<I : Any>(private val source: RStream<I>,
                                    private val predicate: (I) -> Boolean) : RStream<Boolean> {

    override fun subscribe(subscriber: RSubscriber<in Boolean>) {
        source.subscribe(AnySubscriber(subscriber))
    }

    private inner class AnySubscriber(private val subscriber: RSubscriber<in Boolean>): RSubscriber<I> {

        private lateinit var subscription: RSubscription

        override fun onComplete() {
            subscriber.onNext(false)
            subscriber.onComplete()
        }

        override fun onSubscribe(s: RSubscription) {
            subscription = s
            subscriber.onSubscribe(s)
            s.request(Long.MAX_VALUE)
        }

        override fun onNext(item: I) {
            kotlin.runCatching { predicate(item) }
                .onSuccess { passed ->
                    passed.onTrue {
                        subscription.cancel()
                        subscriber.onNext(true)
                        subscriber.onComplete()
                    }
                }.onFailure { onError(it) }
        }

        override fun onError(throwable: Throwable) {
            subscription.cancel()
            subscriber.onError(throwable)
        }
    }
}
