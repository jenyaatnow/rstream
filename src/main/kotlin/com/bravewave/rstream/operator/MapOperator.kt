package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription

internal class MapOperator<I : Any, O : Any>(private val source: RStream<I>,
                                             private val mapper: (I) -> O) : RStream<O> {

    override fun subscribe(subscriber: RSubscriber<in O>) {
        source.subscribe(MapSubscriber(subscriber))
    }

    private inner class MapSubscriber(private val subscriber: RSubscriber<in O>) : RSubscriber<I> {

        private lateinit var subscription: RSubscription

        override fun onComplete() {
            subscriber.onComplete()
        }

        override fun onSubscribe(s: RSubscription) {
            subscription = s
            subscriber.onSubscribe(s)
        }

        override fun onNext(item: I) {
            kotlin.runCatching {
                val result = mapper(item)
                subscriber.onNext(result)
            }.onFailure { onError(it) }
        }

        override fun onError(throwable: Throwable) {
            subscription.cancel()
            subscriber.onError(throwable)
        }
    }
}
