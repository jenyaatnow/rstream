package com.bravewave.rstream.operator

import com.bravewave.rstream.RStream
import com.bravewave.rstream.RSubscriber
import com.bravewave.rstream.RSubscription
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

internal class LogOperator<I : Any>(private val source: RStream<I>) : RStream<I> {

    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")

    private fun printMsg(msg: String) {
        // fixme replace with logger
        println("[${Thread.currentThread().name}] ${LocalDateTime.now().format(formatter)}: $msg")
    }

    override fun subscribe(subscriber: RSubscriber<in I>) {
        printMsg("subscribe()")
        source.subscribe(LogSubscriber(subscriber))
    }

    private inner class LogSubscriber(private val subscriber: RSubscriber<in I>) : RSubscriber<I> {

        override fun onComplete() {
            printMsg("onComplete()")
            subscriber.onComplete()
        }

        override fun onSubscribe(s: RSubscription) {
            printMsg("onSubscribe()")
            subscriber.onSubscribe(LogSubscription(s))
        }

        override fun onNext(item: I) {
            printMsg("onNext($item)")
            subscriber.onNext(item)
        }

        override fun onError(throwable: Throwable) {
            printMsg("onError ($throwable)")
            subscriber.onError(throwable)
        }
    }

    private inner class LogSubscription(private val subscription: RSubscription) : RSubscription {
        
        override fun cancel() {
            printMsg("cancel()")
            subscription.cancel()
        }

        override fun request(n: Long) {
            printMsg("request($n)")
            subscription.request(n)
        }

        override fun resume(n: Long) {
            printMsg("resume($n)")
            subscription.resume(n)
        }
    }
}
