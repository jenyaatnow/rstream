package com.bravewave.rstream.utils

inline fun Boolean.onTrue(action: () -> Unit): Boolean {
    if (this) action()
    return this
}

inline fun Boolean.onFalse(action: () -> Unit): Boolean {
    if (!this) action()
    return this
}

inline fun Boolean.either(onTrue: () -> Unit, onFalse: () -> Unit) {
    if (this) {
        onTrue()
    } else {
        onFalse()
    }
}
