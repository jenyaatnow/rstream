package com.bravewave.rstream

import java.util.concurrent.Flow

interface RSubscriber<T> : Flow.Subscriber<T> {

    fun onSubscribe(s: RSubscription)

    override fun onSubscribe(s: Flow.Subscription?) {
        onSubscribe(s as RSubscription)
    }
}
