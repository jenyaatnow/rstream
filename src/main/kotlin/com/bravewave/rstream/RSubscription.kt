package com.bravewave.rstream

import java.util.concurrent.Flow

interface RSubscription : Flow.Subscription {

    fun resume(n: Long)
}
